<!--
# README first!

This template covers all of the steps required to do a release of the GitLab Performance Tool.
Issues should only be raised with this template by a GPT maintainer who is about to do a release.
-->

# Release steps

- [ ] Confirm no further major MRs are incoming
- [ ] Update project dependencies to use the latest versions:
  - Latest [k6 release](https://github.com/grafana/k6/releases) in [Dockerfile.gpt](../../Dockerfile.gpt#L4) and [lib/run_k6.rb#L18](../../lib/run_k6.rb#L18)
  - [Gemfile.lock](../../Gemfile.lock) with `bundle update`
- [ ] Update any version strings in repo
  - [ ] For GPT:
    - [`bin/run-k6`](../../bin/run-k6)
    - [GPT documentation](../../docs/k6.md)
  - [ ] For GPT Data Generator:
    - [`bin/generate-gpt-data`](../../bin/generate-gpt-data)
    - [GPT Data Generator documentation](../../docs/environment_prep.md)
- [ ] Ensure that performance pipelines with latest `main` changes are passing as expected in `#gpt-performance-run`.
- [ ] Create Release notes - follow similar style as [previous releases](https://gitlab.com/gitlab-org/quality/performance/-/releases). Use Compare revision link to go through and collect all notes from last release SHA - `https://gitlab.com/gitlab-org/quality/performance/-/compare/<previous_version>...main`.
  - Use previous releases for formatting.
    - Omit adding notes for internal changes (CI configurations, wiki reports)
    - Group merge requests for threshold updates in a single line
    - Ensure that change descriptions are end-user friendly (focusing on result of the change, not technical details)
    - Cite non-maintainer authors
  - Use code blocks for pasting draft Release notes
- [ ] Mention GPT maintainers to review and approve Release notes.
- [ ] Mention GPT maintainers to confirm date and time for creating tag and release. Tag and release should be created within the same day. Avoiding Friday releases if possible.
- [ ] Create a _lightweight_ tag with no specific notes - https://gitlab.com/gitlab-org/quality/performance/-/tags.
  - [ ] For GPT release: ensure that pipeline triggered by tag is passing - https://gitlab.com/gitlab-org/quality/performance/-/pipelines
  - [ ] For Data Generator release: trigger `Build GPT Data Generator Docker Image` scheduled pipeline and ensure it's passing
- [ ] Create release
  - [ ] Select the tag on the release page
  - [ ] Fill in release notes
  - [ ] Link in the Docker registries (links will be the same as previous releases)
- [ ] Announce release in `#gitlab_performance_tool` and `#test-platform` Slack channels as well as the Engineering week-in-review

/label ~"type::maintenance" ~"maintenance::release" ~"team::Self-Managed Platform"
