# GitLab Performance Tool - Documentation

For those looking to get started quickly, check out our [Quick Start Guide](quick_start.md).

For detailed information about using the GitLab Performance Tool, please refer to:

* [Preparing the Environment](environment_prep.md)
* [Running the Tests](k6.md)
