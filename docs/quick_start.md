# GitLab Performance Tool - Quick Start Guide

This quickstart guide helps you run basic performance tests with the GitLab Performance Tool (GPT) using Docker (recommended method).
This guide helps you run GitLab performance tests by setting up the required test data under a `gpt` group and executing tests across web, API, and Git endpoints.

- Note - Running performance tests on active GitLab production instances is **not recommended** due to potential operational instability.

## Prerequisites

- Docker installed
- A GitLab instance (v12.5 or higher)
- Admin access to the GitLab instance
- Minimum hardware requirements: 1 CPU Core and 1GB RAM for 200 RPS. See [Hardware Requirements for running the tool](k6.md#hardware) for other configurations
- It is recommended to run the tool as close as possible physically to the GitLab environment and in optimum network conditions
  - [Network Requirements](k6.md#location-and-network-conditions)

## Setup

### 1. Create Admin User & Access Token

1. [Create a new Admin user](https://docs.gitlab.com/user/profile/account/create_accounts/) on your GitLab instance with the ["Can create group"](https://docs.gitlab.com/administration/admin_area/#prevent-a-user-from-creating-top-level-groups) checkbox enabled
2. [Generate a Personal Access Token](https://docs.gitlab.com/user/profile/personal_access_tokens/#create-a-personal-access-token) with `api` scope

### 2. Prepare Environment Config

Create a new directory for your test files and results

```bash
mkdir -p config/environments
mkdir -p results
```

Create an environment config file `config/environments/my-env.json`. You may need to update these required fields:

- `environment.name`: A descriptive label for your instance - used in test reports
- `environment.url`: URL of your GitLab instance
- `environment.user`: Your GitLab admin user with API access
- `environment.storage_nodes`: Use `["default"]` if you have a single [Gitaly Shard](https://docs.gitlab.com/ee/administration/gitaly/) node or [Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/praefect.html). For multiple Gitaly Shards or Clusters, list all storage nodes to distribute test data evenly.

For additional configuration options, see [Preparing the Environment](environment_prep.md#preparing-the-environment-file).

```json
{
  "environment": {
    "name": "my-gitlab-instance",
    "url": "http://gitlab.example.com",
    "user": "your-gpt-admin-username",
    "config": {
      "latency": "0"
    },
    "storage_nodes": ["default"]
  },
  "gpt_data": {
    "root_group": "gpt",
    "large_projects": {
      "group": "large_projects",
      "project": "gitlabhq"
    },
    "many_groups_and_projects": {
      "group": "many_groups_and_projects",
      "subgroups": 1000,
      "subgroup_prefix": "gpt-subgroup-",
      "projects": 10,
      "project_prefix": "gpt-project-"
    }
  }
}
```

### 3. Generate Test Data

```bash
# Run the data generator
docker run -it \
  -e ACCESS_TOKEN=your-access-token \
  -v $(pwd)/config:/config \
  -v $(pwd)/results:/results \
  gitlab/gpt-data-generator --environment my-env.json
```

Expected output:

```text
GPT Data Generator v2.15.0 - opinionated test data for the GitLab Performance Tool
Checking that GitLab environment 'http://gitlab.example.com' is available...
Environment and Access Token check complete - URL: http://gitlab.example.com Version: 17.x.x
Creating group gpt
Creating group gpt/many_groups_and_projects
Creating 500 groups with name prefix 'gpt-subgroup-' under parent group 'gpt/many_groups_and_projects'
Generating groups: 500 from 500 |=========================================================>|

Creating 10 projects each under 500 subgroups with name prefix 'gpt-project-'
Generating projects: 5000 from 5000 |=====================================================>|

| Horizontal data: successfully generated!

| Vertical data: importing large projects for GPT...
Starting import of Project 'gitlabhq1'...
Project has successfully imported:
http://gitlab.example.com/gpt/large_projects/gitlabhq1

| Vertical data: successfully generated!
█ GPT data generation finished successfully.
```

Wait for data generation to complete (may take 1-2 hours).

If you are encountering errors while generating test data, some common issues are outlined in the [Troubleshooting](k6.md#troubleshooting) guide.

### 4. Run Performance Tests

1. Select the appropriate load option based on your GitLab instance's user count.  
For additional predefined options, see [Running the Tests](k6.md#selecting-which-options-file-to-use) documentation.
  
    | Users  | Recommended Option | API/Web/Git RPS |
    |--------|--------------------|-----------------|
    | 1,000  | `60s_20rps.json`   | 20/2/2          |
    | 2,000  | `60s_40rps.json`   | 40/4/4          |
    | 5,000  | `60s_100rps.json`  | 100/10/10       |
    | 10,000 | `60s_200rps.json`  | 200/20/20       |
    | 25,000 | `60s_500rps.json`  | 500/50/50       |
    | 50,000 | `60s_1000rps.json` | 1000/100/100    |

2. Run the tests with your selected option:

```bash
# Run the tests
docker run -it \
  -e ACCESS_TOKEN=your-access-token \
  -v $(pwd)/config:/config \
  -v $(pwd)/results:/results \
  gitlab/gitlab-performance-tool \
  --environment my-env.json \
  --options <option>.json
```

Above command will execute a full test including all [tests](https://gitlab.com/gitlab-org/quality/performance/blob/main/k6/tests), which can take more than an hour to complete.  
To run a shorter subset of tests you can specify subset (`api|web|git`) using the `--tests` flag, e.g. `--tests api` or a single test: `--tests api_v4_groups_projects.js`.

If you are encountering errors while running the tests, some common issues are outlined in the [Troubleshooting](k6.md#troubleshooting) guide.

## 5. Understanding Results

A successful test run will show output like this:

```text
* Environment:                gitlab-example-instance-10k
* Environment Version:        17.4.0-pre `aae05602b3a`
* Option:                     60s_200rps
* Date:                       2024-09-04
* Run Time:                   1h 37m 10.6s (Start: 05:11:18 UTC, End: 06:48:28 UTC)
* GPT Version:                v2.15.0
❯ Overall Results Score: 96.69%
NAME                                                     | RPS   | RPS RESULT           | TTFB AVG  | TTFB P90              | REQ STATUS     | RESULT
---------------------------------------------------------|-------|----------------------|-----------|-----------------------|----------------|--------
api_v4_groups                                            | 200/s | 192.43/s (>128.00/s) | 468.76ms  | 765.55ms (<1700ms)    | 100.00% (>99%) | Passed¹
api_v4_groups_group                                      | 200/s | 24.9/s (>8.00/s)     | 7363.54ms | 15315.78ms (<24000ms) | 100.00% (>20%) | Passed¹
[...more test results...]
```

- Check the console output for test results summary
- Detailed results will be in the `results` directory

Performance test thresholds expect a GitLab instance that's properly sized for its workload. Our [Reference Architectures](https://docs.gitlab.com/administration/reference_architectures/) provide proven configurations, but your instance may achieve similar or better performance with different hardware or customizations. If you run these tests in development environments (like GDK) or non-production setups, adjust your performance expectations accordingly.

Key metrics to look for:

- `Overall Results Score` above 90% indicates good performance
- Green "Passed" results in the rightmost column
- `RPS RESULT` showing achieved rate close to target rate
- `TTFB P90` [percentile](https://en.wikipedia.org/wiki/Percentile_rank) of [Time To First Byte (TTFB)](https://en.wikipedia.org/wiki/Time_to_first_byte) response time is below its threshold
- `REQ STATUS` close to 100% showing successful requests

For more details documentation on how to analyse reports see the [Test Output and Results](k6.md#test-output-and-results) sections of our documentation

## 6. Cleanup

To remove all test data after you're done:

```bash
docker run -it \
  -e ACCESS_TOKEN=your-access-token \
  -v $(pwd)/config:/config \
  gitlab/gpt-data-generator --environment my-env.json --clean-up
```

## Detailed Documentation

For more detailed information about:

- Advanced configuration options
- Custom test scenarios
- Detailed troubleshooting
- Performance tuning recommendations
- Result analysis

Please refer to the full documentation for further reading:

- [Preparing the Environment](environment_prep.md)
- [Running the Tests](k6.md)
- [Current Test Details](https://gitlab.com/gitlab-org/quality/performance/wikis/current-test-details)
