# frozen_string_literal: true

require 'rspec'
require_relative '../lib/run_k6'

RSpec.describe RunK6 do
  describe '.setup_k6' do
    let(:k6_version) { '11.12.13' }
    let(:temp_dir) { Dir.tmpdir }
    let(:fake_k6_path) { File.join(temp_dir, 'k6') }
    let(:gpt_common) { class_double(GPTCommon).as_stubbed_const(verify: true) }

    before do
      ENV['K6_VERSION'] = k6_version
      ENV.delete('K6_URL')
      FileUtils.rm_f(fake_k6_path)
      allow(Open3).to receive(:capture2e).and_return(['', instance_double(Process::Status, success?: true)])
    end

    after do
      ENV.delete('K6_VERSION')
    end

    it 'uses existing system installation if version matches' do
      allow(Open3).to receive(:capture2e).with('k6 version;').and_return(["k6 v#{k6_version}", instance_double(Process::Status, success?: true)])
      allow(Open3).to receive(:capture2e).with('which k6;').and_return(['/usr/local/bin/k6', instance_double(Process::Status, success?: true)])

      expect(described_class.setup_k6).to eq('/usr/local/bin/k6')
    end

    it 'uses existing temp directory installation if version matches' do
      allow(Open3).to receive(:capture2e).with('k6 version;').and_return(['k6 v0.49.0', instance_double(Process::Status, success?: true)])
      allow(Open3).to receive(:capture2e).with("#{fake_k6_path} version;").and_return(["k6 v#{k6_version}\n", instance_double(Process::Status, success?: true)])
      allow(Open3).to receive(:capture2e).with("which #{fake_k6_path};").and_return([fake_k6_path, instance_double(Process::Status, success?: true)])

      expect(described_class.setup_k6).to eq(fake_k6_path)
    end

    shared_examples 'platform download behavior' do |os, cpu, expected_os, expected_arch, archive_type|
      let(:k6_version) { '11.12.13' }
      let(:archive_path) { File.join(Dir.tmpdir, "k6.#{archive_type}") }
      let(:tempfile) { instance_double(Tempfile, path: archive_path) }

      before do
        allow(Open3).to receive(:capture2e).and_return(['', instance_double(Process::Status, success?: true)])
        allow(gpt_common).to receive(:download_file).and_return(tempfile)
        ENV['K6_VERSION'] = k6_version
      end

      it "downloads correct binary for #{expected_os}-#{expected_arch}" do
        allow(OS).to receive_messages(linux?: os == 'linux', mac?: os == 'macos', host_cpu: cpu)

        described_class.setup_k6

        expected_url = "https://github.com/grafana/k6/releases/download/v#{k6_version}/k6-v#{k6_version}-#{expected_os}-#{expected_arch}.#{archive_type}"
        expect(gpt_common).to have_received(:download_file).with(url: expected_url)
      end
    end

    context 'with platform-specific downloads' do
      include_examples 'platform download behavior', 'linux', 'x86_64', 'linux', 'amd64', 'tar.gz'
      include_examples 'platform download behavior', 'macos', 'aarch64', 'macos', 'arm64', 'zip'
    end

    context 'with configuration options' do
      before do
        allow(OS).to receive_messages(linux?: true, host_cpu: 'x86_64')

        # Stub version checks to indicate k6 is not installed
        allow(Open3).to receive(:capture2e).with('k6 version;')
          .and_return(['k6 v0.48.0', instance_double(Process::Status, success?: true)])
        allow(Open3).to receive(:capture2e).with("#{fake_k6_path} version;")
          .and_return(['k6 v0.48.0', instance_double(Process::Status, success?: true)])

        allow(GPTCommon).to receive(:download_file)
          .and_return(instance_double(Tempfile, path: File.join(temp_dir, 'k6.tar.gz')))

        allow(Open3).to receive(:capture2e)
          .with('tar', '-xzvf', anything, '-C', anything, '--strip-components', '1')
           .and_return(['', instance_double(Process::Status, success?: true)])

        allow(gpt_common).to receive(:download_file).and_return(instance_double(Tempfile, path: fake_k6_path))
      end

      it 'can download a specific version' do
        ENV['K6_VERSION'] = '0.49.0'

        expected_url = "https://github.com/grafana/k6/releases/download/v0.49.0/k6-v0.49.0-linux-amd64.tar.gz"
        described_class.setup_k6
        expect(gpt_common).to have_received(:download_file).with(url: expected_url)
      end

      it 'can download from custom URL' do
        custom_url = 'https://custom-domain.com/k6-binary'
        ENV['K6_URL'] = custom_url
        described_class.setup_k6
        expect(gpt_common).to have_received(:download_file).with(url: custom_url)
      end
    end

    context 'with unsupported CPU' do
      before do
        allow(OS).to receive(:host_cpu).and_return('foobar')
      end

      it 'raises error' do
        expect { described_class.setup_k6 }.to raise_error(/CPU type foobar is unsupported/)
      end
    end

    context 'when extraction fails' do
      before do
        allow(OS).to receive_messages(linux?: true, host_cpu: 'x86_64')

        # Stub version checks to indicate k6 is not installed
        allow(Open3).to receive(:capture2e).with('k6 version;')
          .and_return(['k6 v0.48.0', instance_double(Process::Status, success?: true)])
        allow(Open3).to receive(:capture2e).with("#{fake_k6_path} version;")
          .and_return(['k6 v0.48.0', instance_double(Process::Status, success?: true)])
        allow(GPTCommon).to receive(:download_file)
          .and_return(instance_double(Tempfile, path: File.join(temp_dir, 'k6.tar.gz')))

        allow(Open3).to receive(:capture2e)
          .with('tar', '-xzvf', anything, '-C', anything, '--strip-components', '1')
          .and_return(['extraction failed', instance_double(Process::Status, success?: false)])
      end

      it 'raises error' do
        expect { described_class.setup_k6 }.to raise_error(/k6 archive extract failed/)
      end
    end
  end

  describe '.get_env_version' do
    let(:env_url) { 'https://gitlab.example.com' }
    let(:headers) { { 'PRIVATE-TOKEN': 'test-token' } }
    let(:api_url) { "#{env_url}/api/v4/version" }

    before do
      ENV['ACCESS_TOKEN'] = 'test-token'
    end

    after do
      ENV['ACCESS_TOKEN'] = nil
    end

    context 'when request is successful' do
      let(:response_body) { { version: '1.2.3', revision: 'abc123' }.to_json }
      let(:http_response) do
        instance_double(
          HTTP::Response,
          status: instance_double(HTTP::Response::Status, success?: true),
          body: instance_double(HTTP::Response::Body, to_s: response_body)
        )
      end

      before do
        allow(GPTCommon).to receive(:make_http_request)
          .with(
            method: 'get',
            url: api_url,
            headers: headers,
            fail_on_error: false
          )
          .and_return(http_response)
      end

      it 'returns parsed version information' do
        result = described_class.get_env_version(env_url: env_url)
        expect(result).to eq({ "version" => "1.2.3", "revision" => "abc123" })
      end
    end

    context 'when request fails' do
      let(:http_response) do
        instance_double(
          HTTP::Response,
          status: instance_double(HTTP::Response::Status, success?: false)
        )
      end

      before do
        allow(GPTCommon).to receive(:make_http_request)
          .with(
            method: 'get',
            url: api_url,
            headers: headers,
            fail_on_error: false
          )
          .and_return(http_response)
      end

      it 'returns placeholder version information' do
        result = described_class.get_env_version(env_url: env_url)
        expect(result).to eq({ "version" => "-", "revision" => "-" })
      end
    end
  end

  describe '.get_options_env_vars' do
    let(:options_file) { 'path/to/options.json' }

    context 'with basic options file' do
      let(:options_content) do
        {
          'rps' => 100,
          'stages' => [
            { 'duration' => '30s' },
            { 'duration' => '1m' }
          ]
        }
      end

      before do
        allow(File).to receive(:read).with(options_file).and_return(options_content.to_json)
      end

      it 'calculates environment variables correctly' do
        result = described_class.get_options_env_vars(options_file: options_file)

        expect(result['OPTION_RPS']).to eq('100')
        expect(result['OPTION_RPS_COUNT']).to eq('9000') # (30 + 60) * 100
        expect(result['OPTION_STAGES']).to eq(options_content['stages'].to_json)
        expect(result['OPTION_MAX_VUS']).to eq('500') # 100 * 5
      end
    end

    context 'with custom max_vus specified' do
      let(:options_content) do
        {
          'rps' => 100,
          'stages' => [
            { 'duration' => '30s' }
          ],
          'max_vus' => 300
        }
      end

      before do
        allow(File).to receive(:read).with(options_file).and_return(options_content.to_json)
      end

      it 'uses specified max_vus value' do
        result = described_class.get_options_env_vars(options_file: options_file)
        expect(result['OPTION_MAX_VUS']).to eq('300')
      end
    end

    context 'with high rps value' do
      let(:options_content) do
        {
          'rps' => 1000,
          'stages' => [
            { 'duration' => '30s' }
          ]
        }
      end

      before do
        allow(File).to receive(:read).with(options_file).and_return(options_content.to_json)
      end

      it 'caps max_vus at 2000' do
        result = described_class.get_options_env_vars(options_file: options_file)
        expect(result['OPTION_MAX_VUS']).to eq('2000') # not 5000 (1000 rps * 5)
      end
    end

    context 'with config using using seconds for durations' do
      let(:options_content) do
        {
          'rps' => 250,
          'stages' => [
            { 'duration' => '5s', 'target' => 250 },
            { 'duration' => '100s', 'target' => 250 },
            { 'duration' => '5s', 'target' => 0 }
          ],
          'batchPerHost' => 0
        }
      end

      before do
        allow(File).to receive(:read).with(options_file).and_return(options_content.to_json)
      end

      it 'calculates environment variables correctly' do
        result = described_class.get_options_env_vars(options_file: options_file)

        expect(result['OPTION_RPS']).to eq('250')
        expect(result['OPTION_RPS_COUNT']).to eq('27500') # (5 + 100 + 5) * 250
        expect(result['OPTION_STAGES']).to eq(options_content['stages'].to_json)
        expect(result['OPTION_MAX_VUS']).to eq('1250') # 250 * 5
      end
    end

    context 'with mixed duration formats' do
      let(:options_content) do
        {
          'rps' => 50,
          'stages' => [
            { 'duration' => '30s' },
            { 'duration' => '2m' },
            { 'duration' => '90s' }
          ]
        }
      end

      before do
        allow(File).to receive(:read).with(options_file).and_return(options_content.to_json)
      end

      it 'correctly calculates total duration for RPS count' do
        result = described_class.get_options_env_vars(options_file: options_file)
        # 30s + 120s + 90s = 240s total duration
        expect(result['OPTION_RPS_COUNT']).to eq('12000') # 240 * 50
      end
    end
  end

  describe '.setup_env_file_vars' do
    let(:k6_dir) { '/path/to/k6' }
    let(:env_file) { 'test_env.json' }
    let(:env_file_content) do
      {
        'environment' => {
          'name' => 'test-env',
          'url' => 'http://test.example.com/', # note trailing slash
          'user' => 'test-user',
          'config' => {
            'latency' => '50ms'
          }
        },
        'gpt_data' => {
          'root_group' => 'foobar-test-group',
          'skip_check_version' => 'true'
        }
      }
    end

    before do
      allow(File).to receive(:read).with(env_file).and_return(env_file_content.to_json)

      allow(GPTPrepareTestData).to receive_messages(
        prepare_vertical_json_data: 'vertical_data',
        prepare_horizontal_json_data: 'horizontal_data',
        vulnerabilities_projects_group: 'vulnerabilities_group'
      )
    end

    context 'when using values from env_file' do
      it 'sets up environment variables correctly' do
        result = described_class.setup_env_file_vars(k6_dir: k6_dir, env_file: env_file)

        expect(result['ENVIRONMENT_NAME']).to eq('test-env')
        expect(result['ENVIRONMENT_URL']).to eq('http://test.example.com') # Note: no trailing slash
        expect(result['ENVIRONMENT_USER']).to eq('test-user')
        expect(result['ENVIRONMENT_LATENCY']).to eq('50ms')
        expect(result['ENVIRONMENT_ROOT_GROUP']).to eq('foobar-test-group')
        expect(result['ENVIRONMENT_LARGE_PROJECTS']).to eq('vertical_data')
        expect(result['ENVIRONMENT_MANY_GROUPS_AND_PROJECTS']).to eq('horizontal_data')
        expect(result['ENVIRONMENT_VULNERABILITIES_GROUP']).to eq('vulnerabilities_group')
        expect(result['GPT_LARGE_PROJECT_CHECK_SKIP']).to eq('true')

        # Verify GPTPrepareTestData calls
        expect(GPTPrepareTestData).to have_received(:prepare_vertical_json_data)
          .with(k6_dir: k6_dir, env_file_vars: env_file_content)
        expect(GPTPrepareTestData).to have_received(:prepare_horizontal_json_data)
          .with(env_file_vars: env_file_content)
        expect(GPTPrepareTestData).to have_received(:vulnerabilities_projects_group)
          .with(env_file_vars: env_file_content)
      end
    end

    context 'when using values from ENV' do
      before do
        ENV['ENVIRONMENT_NAME'] = 'env-override-name'
        ENV['ENVIRONMENT_URL'] = 'http://env.example.com'
        ENV['ENVIRONMENT_USER'] = 'env-user'
        ENV['ENVIRONMENT_LATENCY'] = '100ms'
        ENV['GPT_LARGE_PROJECT_CHECK_SKIP'] = 'false'
      end

      after do
        ENV.delete('ENVIRONMENT_NAME')
        ENV.delete('ENVIRONMENT_URL')
        ENV.delete('ENVIRONMENT_USER')
        ENV.delete('ENVIRONMENT_LATENCY')
        ENV.delete('GPT_LARGE_PROJECT_CHECK_SKIP')
      end

      it 'prefers ENV values over env_file values' do
        result = described_class.setup_env_file_vars(k6_dir: k6_dir, env_file: env_file)

        expect(result['ENVIRONMENT_NAME']).to eq('env-override-name')
        expect(result['ENVIRONMENT_URL']).to eq('http://env.example.com')
        expect(result['ENVIRONMENT_USER']).to eq('env-user')
        expect(result['ENVIRONMENT_LATENCY']).to eq('100ms')
        expect(result['GPT_LARGE_PROJECT_CHECK_SKIP']).to eq('false')
      end
    end

    context 'when env_file is using default values' do
      let(:env_file_content) do
        {
          'environment' => {
            'name' => 'default-config',
            'url' => 'http://test.example.com/',
            'user' => 'test-user',
            'config' => {}
          },
          'gpt_data' => {}
        }
      end

      it 'has a default latency value of nil' do
        result = described_class.setup_env_file_vars(k6_dir: k6_dir, env_file: env_file)
        expect(result['ENVIRONMENT_LATENCY']).to be_nil
      end
    end
  end

  describe '.setup_env_vars' do
    let(:k6_dir) { '/path/to/k6' }
    let(:env_file) { 'test_env.json' }
    let(:options_file) { 'test_options.json' }

    before do
      allow(GPTCommon).to receive_messages(
        check_gitlab_env_and_token: true,
        get_license_plan: 'ultimate'
      )

      allow(described_class).to receive(:setup_env_file_vars)
                                  .with(k6_dir: k6_dir, env_file: env_file)
                                  .and_return({
                                    'ENVIRONMENT_NAME' => 'test-env',
                                    'ENVIRONMENT_URL' => 'http://test.example.com'
                                  })

      allow(described_class).to receive(:get_options_env_vars)
                                  .with(options_file: options_file)
                                  .and_return({
                                    'OPTION_RPS' => '250',
                                    'OPTION_MAX_VUS' => '1250'
                                  })

      allow(described_class).to receive(:get_env_version)
                                  .and_return({
                                    'version' => '16.0.0',
                                    'revision' => 'abc123'
                                  })

      allow(described_class).to receive(:setup_env_config_vars).and_call_original
    end

    context 'when using default values' do
      it 'sets up environment variables with defaults' do
        result = described_class.setup_env_vars(k6_dir: k6_dir, env_file: env_file, options_file: options_file)

        expect(result['RPS_THRESHOLD_MULTIPLIER']).to eq('0.8')
        expect(result['SUCCESS_RATE_THRESHOLD']).to eq('0.99')
        expect(result['TTFB_THRESHOLD']).to eq('200')

        expect(result['GIT_PULL_ENDPOINT_THROUGHPUT']).to eq('0.1')
        expect(result['GIT_CLONE_ENDPOINT_THROUGHPUT']).to eq('0.04')
        expect(result['GIT_PUSH_ENDPOINT_THROUGHPUT']).to eq('0.02')
        expect(result['WEB_ENDPOINT_THROUGHPUT']).to eq('0.1')
        expect(result['SCENARIO_ENDPOINT_THROUGHPUT']).to eq('0.004')

        expect(result['K6_SETUP_TIMEOUT']).to eq('60s')
        expect(result['K6_TEARDOWN_TIMEOUT']).to eq('60s')

        expect(result['ENVIRONMENT_NAME']).to eq('test-env')
        expect(result['ENVIRONMENT_URL']).to eq('http://test.example.com')

        expect(result['OPTION_RPS']).to eq('250')
        expect(result['OPTION_MAX_VUS']).to eq('1250')
      end
    end

    context 'when using ENV overrides' do
      before do
        ENV['RPS_THRESHOLD_MULTIPLIER'] = '0.9'
        ENV['SUCCESS_RATE_THRESHOLD'] = '0.95'
        ENV['TTFB_THRESHOLD'] = '300'
        ENV['GIT_PULL_ENDPOINT_THROUGHPUT'] = '0.2'
        ENV['K6_SETUP_TIMEOUT'] = '120s'
      end

      it 'uses ENV values instead of defaults' do
        result = described_class.setup_env_vars(k6_dir: k6_dir, env_file: env_file, options_file: options_file)

        expect(result['RPS_THRESHOLD_MULTIPLIER']).to eq('0.9')
        expect(result['SUCCESS_RATE_THRESHOLD']).to eq('0.95')
        expect(result['TTFB_THRESHOLD']).to eq('300')
        expect(result['GIT_PULL_ENDPOINT_THROUGHPUT']).to eq('0.2')
        expect(result['K6_SETUP_TIMEOUT']).to eq('120s')

        expect(result['GIT_PUSH_ENDPOINT_THROUGHPUT']).to eq('0.02')
        expect(result['K6_TEARDOWN_TIMEOUT']).to eq('60s')
      end
    end

    it 'calls setup_env_config_vars with merged environment variables' do
      described_class.setup_env_vars(k6_dir: k6_dir, env_file: env_file, options_file: options_file)

      expect(described_class).to have_received(:setup_env_config_vars) do |env_vars|
        expect(env_vars).to include(
          'RPS_THRESHOLD_MULTIPLIER' => '0.8',
          'ENVIRONMENT_NAME' => 'test-env',
          'OPTION_RPS' => '250'
        )
      end
    end
  end

  describe '.setup_env_config_vars' do
    let(:env_vars) do
      {
        'ENVIRONMENT_URL' => 'http://test.example.com',
        'SOME_OTHER_VAR' => 'value'
      }
    end

    let(:version_info) do
      {
        'version' => '16.0.0',
        'revision' => 'abc123'
      }
    end

    before do
      allow(described_class).to receive(:get_env_version).and_return(version_info)
      allow(GPTCommon).to receive_messages(check_gitlab_env_and_token: true, get_license_plan: 'ultimate')
    end

    context 'when GPT_SKIP_ENV_CHECK is not set' do
      it 'checks GitLab environment and sets up config vars' do
        result = described_class.setup_env_config_vars(env_vars)

        expect(GPTCommon).to have_received(:check_gitlab_env_and_token)
          .with(env_url: 'http://test.example.com')
        expect(described_class).to have_received(:get_env_version)
          .with(env_url: 'http://test.example.com')
        expect(GPTCommon).to have_received(:get_license_plan)
          .with(env_url: 'http://test.example.com')

        expect(result).to include(
          'ENVIRONMENT_URL' => 'http://test.example.com',
          'ENVIRONMENT_VERSION' => '16.0.0',
          'ENVIRONMENT_REVISION' => 'abc123',
          'ENVIRONMENT_LICENSE' => 'ultimate',
          'SOME_OTHER_VAR' => 'value'
        )
      end
    end

    context 'when GPT_SKIP_ENV_CHECK is set' do
      before do
        ENV['GPT_SKIP_ENV_CHECK'] = 'true'
      end

      after do
        ENV.delete('GPT_SKIP_ENV_CHECK')
      end

      it 'skips GitLab environment check but sets up other config vars' do
        result = described_class.setup_env_config_vars(env_vars)

        expect(GPTCommon).not_to have_received(:check_gitlab_env_and_token)
        expect(described_class).to have_received(:get_env_version)
          .with(env_url: 'http://test.example.com')
        expect(GPTCommon).to have_received(:get_license_plan)
          .with(env_url: 'http://test.example.com')

        expect(result).to include(
          'ENVIRONMENT_URL' => 'http://test.example.com',
          'ENVIRONMENT_VERSION' => '16.0.0',
          'ENVIRONMENT_REVISION' => 'abc123',
          'ENVIRONMENT_LICENSE' => 'ultimate',
          'SOME_OTHER_VAR' => 'value'
        )
      end
    end

    context 'when preserving existing env_vars' do
      let(:env_vars) do
        {
          'ENVIRONMENT_URL' => 'http://test.example.com',
          'EXISTING_VAR' => 'should_remain',
          'ANOTHER_VAR' => 'also_remains'
        }
      end

      it 'maintains existing variables while adding config vars' do
        result = described_class.setup_env_config_vars(env_vars)

        expect(result).to include(
          'EXISTING_VAR' => 'should_remain',
          'ANOTHER_VAR' => 'also_remains'
        )
      end
    end
  end

  describe ".check_large_projects_visibility" do
    let(:env_vars) do
      {
        'ENVIRONMENT_LARGE_PROJECTS' => '[{"xxx": "group/xxx"}]',
        'ENVIRONMENT_URL' => 'http://example.test'
      }
    end

    let(:gpt_common) { class_spy(GPTCommon) }
    let(:success_response) { instance_double(HTTP::Response, uri: 'https://gitlab.com/group/project') }
    let(:failed_response) { instance_double(HTTP::Response, uri: 'https://gitlab.com/users/sign_in') }
    let(:error_response) { instance_double(HTTP::Response, uri: 'https://gitlab.com/404') }

    before do
      ENV['ACCESS_TOKEN'] = 'test_token'
      stub_const('GPTCommon', gpt_common)
    end

    context "when projects are accessible" do
      before do
        allow(gpt_common).to receive(:make_http_request).and_return(success_response)
      end

      it "does not raise error" do
        expect { described_class.check_large_projects_visibility(env_vars: env_vars) }.not_to raise_error
      end
    end

    context "when a project requires authentication" do
      before do
        allow(gpt_common).to receive(:make_http_request).and_return(failed_response)
      end

      it "raises error" do
        expect { described_class.check_large_projects_visibility(env_vars: env_vars) }
          .to raise_error(RuntimeError, /exists and has Public visibility/)
      end
    end

    # TODO this is questionable, we probably should error here as the project is not accessible for another reason
    context "when a project returns non-sign-in error" do
      before do
        allow(gpt_common).to receive(:make_http_request).and_return(error_response)
      end

      it "does not raise error" do
        expect { described_class.check_large_projects_visibility(env_vars: env_vars) }.not_to raise_error
      end
    end
  end

  describe ".prepare_tests" do
    let(:gpt_prepare_test_data) { class_spy(GPTPrepareTestData) }
    let(:env_vars) { { 'some' => 'value' } }

    before do
      stub_const('GPTPrepareTestData', gpt_prepare_test_data)
    end

    context "when tests include git_push" do
      # note git_push can be anywhere in file path
      let(:tests) { %w[test_git_push_something another_test] }

      it "calls prepare_git_push_data with env_vars" do
        described_class.prepare_tests(tests: tests, env_vars: env_vars)
        expect(gpt_prepare_test_data).to have_received(:prepare_git_push_data).with(env_vars: env_vars)
      end
    end

    context "when tests don't include git_push" do
      let(:tests) { %w[some_other_test another_test] }

      it "does not call prepare_git_push_data" do
        described_class.prepare_tests(tests: tests, env_vars: env_vars)
        expect(gpt_prepare_test_data).not_to have_received(:prepare_git_push_data)
      end
    end

    context "when env_vars is empty" do
      let(:tests) { %w[git_push] }
      let(:env_vars) { {} }

      it "does not call prepare_git_push_data" do
        described_class.prepare_tests(tests: tests, env_vars: env_vars)
        expect(gpt_prepare_test_data).not_to have_received(:prepare_git_push_data)
      end
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers -- complex underlying method with multiple params
  describe '.get_tests' do
    let(:k6_dir) { '/path/to/k6' }
    let(:env_vars) { {} }
    let(:gitlab_env_settings) { {} }
    let(:test_glob_path) { '/path/to/k6/some_test' }
    let(:found_test) { "#{test_glob_path}/test.js" }

    let(:docker_tests_dir) { '/docker/tests' }

    # Test structure under k6_dir
    let(:test_files) do
      {
        # Standard test locations
        "/k6/test1.js" => true,                    # Root js file
        "/k6/test1/basic.js" => true,              # Test dir js file
        "/k6/test1/api/api_test.js" => true,       # API dir
        "/k6/test1/git/git_test.js" => true,       # Git dir
        "/k6/test1/web/web_test.js" => true,       # Web dir
        "/k6/test1/random/ignored.js" => false,    # Random dir (should be ignored)

        # Optional test locations (flag controlled)
        "/k6/test1/quarantined/q_test.js" => true, # Quarantined
        "/k6/test1/experimental/exp_test.js" => true, # Experimental
        "/k6/test1/scenarios/scenario_test.js" => true, # Scenarios

        # Tests directory structure
        "/k6/tests/group1/test1.js" => true,
        "/k6/tests/group2/test2.js" => true,

        # Docker tests directory (when enabled)
        "/docker/tests/group1/test1.js" => true,
        "/docker/tests/group2/test2.js" => true
      }
    end

    before do
      stub_const('ENV', {})
      allow(Dir).to receive(:glob).with(any_args).and_return([])
      allow(File).to receive(:dirname).and_call_original
      allow(File).to receive(:basename).and_call_original
      allow(File).to receive(:extname).and_call_original
    end

    # Basic empty/nil cases
    context "when opts[:tests] is empty" do
      let(:opts) { { tests: [] } }

      it "raises an error" do
        expect do
          described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)
        end.to raise_error(RuntimeError, /No tests found in specified path/)
      end
    end

    context "when test_paths are provided" do
      let(:test_path) { 'test1' }
      let(:opts) { { tests: [test_path] } }
      let(:test_info) { class_spy(TestInfo) }

      before do
        stub_const('TestInfo', test_info)
        allow(test_info).to receive_messages(test_has_unsafe_requests?: false, test_has_flag?: false)
        allow(File).to receive(:open).and_return(StringIO.new(""))
      end

      context "when test_path is a base name" do
        before do
          # First glob finds the directory
          allow(Dir).to receive(:glob).with([
            'test1',
            '/test1', # Empty ENV case
            '/path/to/k6/test1',
            '/path/to/k6/tests/test1'
          ]).and_return(['/path/to/k6/test1'])

          # Finds standard directory js files
          allow(Dir).to receive(:glob)
            .with(%w[/path/to/k6/test1.js /path/to/k6/test1/*.js /path/to/k6/test1/api/*.js /path/to/k6/test1/git/*.js /path/to/k6/test1/web/*.js])
            .and_return(%w[/path/to/k6/test1/basic.js /path/to/k6/test1/api/api_test.js /path/to/k6/test1/git/git_test.js /path/to/k6/test1/web/web_test.js])

          # Special directories
          allow(Dir).to receive(:glob).with('/path/to/k6/test1/quarantined/*.js')
            .and_return(['/path/to/k6/test1/quarantined/q_test.js'])
          allow(Dir).to receive(:glob).with('/path/to/k6/test1/experimental/*.js')
             .and_return(['/path/to/k6/test1/experimental/exp_test.js'])
          allow(Dir).to receive(:glob).with('/path/to/k6/test1/scenarios/*.js')
            .and_return(['/path/to/k6/test1/scenarios/scenario_test.js'])
        end

        it "finds js files in standard directories" do
          result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

          expect(result).to include(
            '/path/to/k6/test1/basic.js',
            '/path/to/k6/test1/api/api_test.js',
            '/path/to/k6/test1/git/git_test.js',
            '/path/to/k6/test1/web/web_test.js'
          )
        end

        context "without special directory flags" do
          let(:opts) { { tests: [test_path], quarantined: false, experimental: false, scenarios: false } }

          it "excludes special directory when flags disabled" do
            result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)
            expect(result).not_to include(
              '/path/to/k6/test1/quarantined/q_test.js',
              '/path/to/k6/test1/experimental/exp_test.js',
              '/path/to/k6/test1/scenarios/scenario_test.js'
            )
          end
        end

        context "with special directory flags" do
          let(:opts) { { tests: [test_path], quarantined: true, experimental: true, scenarios: true } }

          it "includes special directory tests when flags are enabled" do
            result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).to include(
              '/path/to/k6/test1/quarantined/q_test.js',
              '/path/to/k6/test1/experimental/exp_test.js',
              '/path/to/k6/test1/scenarios/scenario_test.js'
            )
          end
        end
      end

      context "when test_path includes js extension" do
        let(:test_path) { 'path/to/specific_test.js' }

        before do
          allow(Dir).to receive(:glob).with('path/to/specific_test.js')
                                      .and_return(['path/to/specific_test.js'])
        end

        it "finds the specific js file" do
          result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

          expect(result).to include('path/to/specific_test.js')
        end
      end

      context "with GPT_DOCKER_TESTS_DIR set" do
        before do
          stub_const('ENV', { 'GPT_DOCKER_TESTS_DIR' => docker_tests_dir })
          allow(Dir).to receive(:glob).with("#{docker_tests_dir}/*/test1.js")
            .and_return(["#{docker_tests_dir}/group1/test1.js"])
        end

        it "includes tests from docker directory" do
          result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

          expect(result).to include("#{docker_tests_dir}/group1/test1.js")
        end
      end

      context "with test filtering" do
        let(:test_path) { 'test1' }
        let(:base_file_content) { StringIO.new("") }
        let(:base_opts) { { tests: [test_path] } }

        before do
          allow(Dir).to receive(:glob).with(any_args).and_return([])
          stub_const('TestInfo', test_info)
          allow(test_info).to receive_messages(test_has_unsafe_requests?: false, test_has_flag?: false)
          allow(File).to receive(:open).and_return(base_file_content)

          # First glob finds directory
          allow(Dir).to receive(:glob)
            .with(%w[test1 /test1 /path/to/k6/test1 /path/to/k6/tests/test1])
            .and_return(['/path/to/k6/test1'])

          # Return some test files
          allow(Dir).to receive(:glob)
            .with(%w[/path/to/k6/test1.js /path/to/k6/test1/*.js /path/to/k6/test1/api/*.js /path/to/k6/test1/git/*.js /path/to/k6/test1/web/*.js])
            .and_return(%w[/path/to/k6/test1/duplicate_name.js /path/to/k6/test1/other/duplicate_name.js /path/to/k6/test1/unsafe_test.js /path/to/k6/test1/vuln_test.js /path/to/k6/test1/normal_test.js])
        end

        context "with duplicate basename" do
          it "keeps only unique basename" do
            result = described_class.get_tests(k6_dir: k6_dir, opts: base_opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            # Should only include one of the duplicate_name.js files
            expect(result&.count { |path| File.basename(path, '.js') == 'duplicate_name' }).to eq(1)
          end
        end

        context "with unsafe requests" do
          before do
            allow(test_info).to receive(:test_has_unsafe_requests?).with('/path/to/k6/test1/unsafe_test.js').and_return(true)
          end

          it "excludes unsafe tests by default" do
            result = described_class.get_tests(k6_dir: k6_dir, opts: base_opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).not_to include('/path/to/k6/test1/unsafe_test.js')
          end

          it "includes unsafe tests when unsafe flag is set" do
            opts = base_opts.merge(unsafe: true)
            result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).to include('/path/to/k6/test1/unsafe_test.js')
          end
        end

        context "with excludes requests" do
          before do
            allow(Dir).to receive(:glob)
              .with(%w[/path/to/k6/test1.js /path/to/k6/test1/*.js /path/to/k6/test1/api/*.js /path/to/k6/test1/git/*.js /path/to/k6/test1/web/*.js])
              .and_return(%w[/path/to/k6/dir/test1.js /path/to/k6/dir/test2.js /path/to/k6/dir/test3.js /path/to/k6/dir/test4.js])
          end

          it "excludes tests using the excludes flag" do
            opts = base_opts.merge(excludes: %w[test2 /path/to/k6/dir/test3.js])
            result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).to include('/path/to/k6/dir/test1.js')
            expect(result).not_to include('/path/to/k6/dir/test2.js')
            expect(result).not_to include('/path/to/k6/dir/test3.js')
            expect(result).to include('/path/to/k6/dir/test4.js')
          end
        end

        context "with vulnerability flags" do
          before do
            allow(test_info).to receive(:test_has_flag?)
              .with('/path/to/k6/test1/vuln_test.js', 'vulnerabilities')
              .and_return(true)
          end

          it "excludes vulnerability tests by default" do
            result = described_class.get_tests(k6_dir: k6_dir, opts: base_opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).not_to include('/path/to/k6/test1/vuln_test.js')
          end

          it "includes vulnerability tests when vulnerabilities flag is set" do
            opts = base_opts.merge(vulnerabilities: true)
            result = described_class.get_tests(k6_dir: k6_dir, opts: opts, env_vars: env_vars, gitlab_env_settings: gitlab_env_settings)

            expect(result).to include('/path/to/k6/test1/vuln_test.js')
          end
        end
      end
    end
  end
  # rubocop:enable RSpec/MultipleMemoizedHelpers

  # rubocop:disable RSpec/MultipleMemoizedHelpers -- complex underlying method with multiple params
  describe ".filter_tests" do
    let(:test_files) { %w[/path/to/k6/test1.js /path/to/k6/test2.js] }
    let(:gitlab_env_settings) { {} }
    let(:test_info) { class_spy(TestInfo) }
    let(:base_env_vars) do
      {
        'ENVIRONMENT_VERSION' => '15.8.0',
        'ENVIRONMENT_LARGE_PROJECTS' => '[{"path": "group/project"}]',
        'GPT_LARGE_PROJECT_CHECK_SKIP' => 'true'
      }
    end

    before do
      stub_const('TestInfo', test_info)
    end

    context "when env_vars is empty" do
      let(:env_vars) { {} }
      let(:gitlab_env_settings) { {} }

      it "returns tests unmodified" do
        result = described_class.filter_tests(
          tests: test_files,
          env_vars: env_vars,
          gitlab_env_settings: gitlab_env_settings,
          rate_limits: nil,
          unattended: nil
        )

        expect(result).to eq(test_files)
      end
    end

    context "with gitlab version filtering" do
      let(:env_vars) { base_env_vars }

      context "when test is supported by version" do
        before do
          test_files.each do |test_file|
            allow(test_info).to receive(:test_supported_by_gitlab_version?)
              .with(test_file, '15.8.0')
              .and_return(true)
          end
        end

        it "keeps supported tests" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(result).to match_array(test_files)
        end
      end

      context "when test is not supported by version" do
        before do
          allow(test_info).to receive(:test_supported_by_gitlab_version?)
            .with('/path/to/k6/test1.js', '15.8.0')
            .and_return(false)
          allow(test_info).to receive(:test_supported_by_gitlab_version?)
            .with('/path/to/k6/test2.js', '15.8.0')
            .and_return(true)
        end

        it "filters out unsupported tests" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(result).to contain_exactly('/path/to/k6/test2.js')
        end
      end
    end

    context "with gitlab settings filtering" do
      let(:env_vars) { base_env_vars.merge('ENVIRONMENT_URL' => 'http://gitlab.test') }
      let(:rate_limits) { true }
      let(:unattended) { false }

      context "when test is supported by gitlab settings" do
        before do
          allow(test_info).to receive(:test_supported_by_gitlab_settings?)
            .with(
              '/path/to/k6/test1.js',
              gitlab_env_settings,
              'http://gitlab.test',
              rate_limits,
              unattended
            ).and_return(true)
          allow(test_info).to receive(:test_supported_by_gitlab_settings?)
            .with(
              '/path/to/k6/test2.js',
              gitlab_env_settings,
              'http://gitlab.test',
              rate_limits,
              unattended
            ).and_return(true)
        end

        it "keeps supported tests" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: rate_limits,
            unattended: unattended
          )

          expect(result).to match_array(test_files)
        end
      end

      context "when test is not supported by gitlab settings" do
        before do
          allow(test_info).to receive(:test_supported_by_gitlab_settings?)
            .with(
              '/path/to/k6/test1.js',
              gitlab_env_settings,
              'http://gitlab.test',
              rate_limits,
              unattended
            ).and_return(false)
          allow(test_info).to receive(:test_supported_by_gitlab_settings?)
            .with(
              '/path/to/k6/test2.js',
              gitlab_env_settings,
              'http://gitlab.test',
              rate_limits,
              unattended
            ).and_return(true)
        end

        it "filters out unsupported tests" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: rate_limits,
            unattended: unattended
          )

          expect(result).to contain_exactly('/path/to/k6/test2.js')
        end
      end
    end

    context "with web_user.js test filtering" do
      let(:test_files) do
        [
          'k6/tests/web/web_user.js', # The special file we care about
          '/path/to/k6/test2.js'
        ]
      end

      let(:env_vars) do
        base_env_vars.merge(
          'ENVIRONMENT_URL' => 'http://gitlab.test',
          'ENVIRONMENT_USER' => 'test_user'
        )
      end

      let(:gpt_common) { class_spy(GPTCommon) }
      let(:http_response) { instance_double(HTTP::Response, status: 200) }

      before do
        stub_const('GPTCommon', gpt_common)
      end

      context "when user page returns 200" do
        before do
          allow(gpt_common).to receive(:make_http_request)
           .with(
             method: 'get',
             url: 'http://gitlab.test/test_user',
             fail_on_error: false,
             follow_redirects: false
           ).and_return(http_response)
        end

        it "keeps web_user.js test" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(result).to include('k6/tests/web/web_user.js')
        end
      end

      context "when user page does not return 200" do
        let(:http_response) { instance_double(HTTP::Response, status: 404) }

        before do
          allow(gpt_common).to receive(:make_http_request)
            .with(
              method: 'get',
              url: 'http://gitlab.test/test_user',
              fail_on_error: false,
              follow_redirects: false
            ).and_return(http_response)
        end

        it "removes web_user.js test" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(result).not_to include('k6/tests/web/web_user.js')
          expect(result).to include('/path/to/k6/test2.js')
        end
      end

      context "when web_user.js is not in test files" do
        let(:test_files) { ['/path/to/k6/test2.js'] }

        it "does not make http request" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(gpt_common).not_to have_received(:make_http_request)
          expect(result).to match_array(test_files)
        end
      end
    end

    context "with large project data filtering" do
      let(:test_files) { %w[/path/to/k6/test1.js /path/to/k6/test2.js] }
      let(:env_vars) do
        base_env_vars.merge(
          'ENVIRONMENT_URL' => 'http://gitlab.test',
          'ENVIRONMENT_LARGE_PROJECTS' => '[{"encoded_path": "group%2Fproject", "unencoded_path": "group/project"}]',
          'GPT_LARGE_PROJECT_CHECK_SKIP' => 'false'
        )
      end

      let(:gpt_common) { class_spy(GPTCommon) }
      let(:http_response) { instance_double(HTTP::Response, body: '{"description": "Version: 1.2.3"}') }

      before do
        stub_const('GPTCommon', gpt_common)
        stub_const('GPTCommon::RequestError', Class.new(StandardError))
        stub_const('ENV', { 'ACCESS_TOKEN' => 'test_token' })
      end

      context "when large project check is successful" do
        before do
          allow(gpt_common).to receive(:make_http_request)
            .with(
              method: 'get',
              url: 'http://gitlab.test/api/v4/projects/group%2Fproject',
              headers: { 'PRIVATE-TOKEN': 'test_token' },
              fail_on_error: true
            ).and_return(http_response)

          allow(test_info).to receive(:test_supported_by_gpt_data?)
            .with('/path/to/k6/test1.js', '1.2.3')
            .and_return(true)
          allow(test_info).to receive(:test_supported_by_gpt_data?)
            .with('/path/to/k6/test2.js', '1.2.3')
            .and_return(false)
        end

        it "filters based on gpt data version" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(result).to contain_exactly('/path/to/k6/test1.js')
        end
      end

      context "when request fails" do
        before do
          allow(gpt_common).to receive(:make_http_request)
            .and_raise(GPTCommon::RequestError.new("API Error"))
        end

        it "raises error with project path info" do
          expected_message = <<~ERROR.chomp
            Large Project request has failed with the error:
            API Error
            Please ensure that Large Project exists at this location 'group/project'
            Exiting...
          ERROR
          expect do
            described_class.filter_tests(
              tests: test_files,
              env_vars: env_vars,
              gitlab_env_settings: gitlab_env_settings,
              rate_limits: nil,
              unattended: nil
            )
          end.to raise_error(RuntimeError, "\n#{expected_message}")
        end
      end

      context "when description cannot be parsed" do
        let(:http_response) { instance_double(HTTP::Response, body: '{"description": null}') }

        before do
          allow(gpt_common).to receive(:make_http_request)
            .with(
              method: 'get',
              url: 'http://gitlab.test/api/v4/projects/group%2Fproject',
              headers: { 'PRIVATE-TOKEN': 'test_token' },
              fail_on_error: true
            ).and_return(http_response)
        end

        it "raises error about description parsing" do
          expect do
            described_class.filter_tests(
              tests: test_files,
              env_vars: env_vars,
              gitlab_env_settings: gitlab_env_settings,
              rate_limits: nil,
              unattended: nil
            )
          end.to raise_error(RuntimeError, /Large Project's description can't be parsed/)
        end
      end

      context "when check is skipped" do
        let(:env_vars) do
          base_env_vars.merge(
            'GPT_LARGE_PROJECT_CHECK_SKIP' => 'true'
          )
        end

        it "does not perform version check" do
          result = described_class.filter_tests(
            tests: test_files,
            env_vars: env_vars,
            gitlab_env_settings: gitlab_env_settings,
            rate_limits: nil,
            unattended: nil
          )

          expect(gpt_common).not_to have_received(:make_http_request)
          expect(result).to match_array(test_files)
        end
      end
    end
  end
  # rubocop:enable RSpec/MultipleMemoizedHelpers

  # rubocop:disable RSpec/MultipleMemoizedHelpers -- complex underlying method with multiple params
  describe '.run_k6' do
    let(:k6_path) { '/usr/local/bin/k6' }
    let(:test_file) { 'test/performance/sample_test.js' }
    let(:results_dir) { 'tmp/k6_results' }
    let(:options_file) { nil }
    let(:env_vars) { { 'ENVIRONMENT_NAME' => 'test' } }
    let(:opts) { {} }
    let(:gpt_version) { '1.2.3' }
    let(:base_params) do
      {
        k6_path: k6_path,
        opts: opts,
        env_vars: env_vars,
        options_file: options_file,
        test_file: test_file,
        results_dir: results_dir,
        gpt_version: gpt_version
      }
    end

    let(:wait_thread) { instance_double(Thread) }
    let(:process_status) { instance_double(Process::Status) }
    let(:stdin) { instance_double(IO) }

    before do
      allow(stdin).to receive(:close)
      allow(wait_thread).to receive(:value).and_return(process_status)
      allow(process_status).to receive_messages(exitstatus: 0, success?: true, signaled?: false)
      allow(Open3).to receive(:popen2e).and_yield(stdin, [].each, wait_thread)
    end

    context 'when constructing command' do
      it 'includes mandatory flags' do
        described_class.run_k6(
          k6_path: k6_path,
          opts: opts,
          env_vars: env_vars,
          options_file: options_file,
          test_file: test_file,
          results_dir: results_dir,
          gpt_version: gpt_version
        )

        expect(Open3).to have_received(:popen2e) do |_, *cmd|
          expect(cmd).to include('--summary-time-unit', 'ms')
          expect(cmd).to include('--summary-trend-stats', 'avg,min,med,max,p(90),p(95)')
          expect(cmd).to include('--user-agent', "GPT/#{gpt_version}")
          expect(cmd).to include('--insecure-skip-tls-verify')
        end
      end

      context 'with optional flags' do
        it 'includes config file when provided' do
          options_file = 'config.json'

          described_class.run_k6(
            k6_path: k6_path,
            opts: opts,
            env_vars: env_vars,
            options_file: options_file,
            test_file: test_file,
            results_dir: results_dir,
            gpt_version: gpt_version
          )

          expect(Open3).to have_received(:popen2e) do |_, *cmd|
            expect(cmd).to include('--config', options_file)
          end
        end

        context 'with influxdb settings' do
          let(:influxdb_url) { 'http://localhost:8086' }

          test_cases = [
            {
              env_set: true,
              url_set: true,
              should_include: true,
              description: 'includes output when both ENV and URL are set'
            },
            {
              env_set: true,
              url_set: false,
              should_include: false,
              description: 'excludes output when only ENV is set'
            },
            {
              env_set: false,
              url_set: true,
              should_include: false,
              description: 'excludes output when only URL is set'
            }
          ]
          test_cases.each do |test_case|
            it test_case[:description] do
              ENV['K6_INFLUXDB_OUTPUT'] = test_case[:env_set] ? '1' : nil
              test_opts = test_case[:url_set] ? { influxdb_url: influxdb_url } : {}

              described_class.run_k6(
                k6_path: k6_path,
                opts: test_opts,
                env_vars: env_vars,
                options_file: options_file,
                test_file: test_file,
                results_dir: results_dir,
                gpt_version: gpt_version
              )

              expect(Open3).to have_received(:popen2e) do |_, *cmd|
                if test_case[:should_include]
                  expect(cmd).to include('--out', "influxdb=#{influxdb_url}")
                else
                  expect(cmd).not_to include('--out')
                end
              end
            end
          end
        end

        context 'with debug mode' do
          it 'includes debug flag when enabled' do
            ENV['GPT_DEBUG'] = '1'
            described_class.run_k6(**base_params)

            expect(Open3).to(have_received(:popen2e)) { |_, *cmd| expect(cmd).to include('--http-debug') }
            expect { described_class.run_k6(**base_params) }.not_to raise_error
          end
        end
      end
    end

    context 'with execution status' do
      context 'when successful' do
        before do
          allow(Open3).to receive(:popen2e).and_yield(stdin, ['test output'].each, wait_thread)
        end

        it 'returns true and captures output' do
          success, output = described_class.run_k6(**base_params)

          expect(success).to be true
          expect(output).to eq(['test output'])
        end
      end

      context 'when failing' do
        before do
          allow(GPTLogger.logger).to receive(:error)
        end

        it 'returns false for no data generated' do
          allow(Open3).to receive(:popen2e).and_yield(stdin, ['No data generated'].each, wait_thread)

          success, output = described_class.run_k6(**base_params)
          expect(success).to be false
          expect(output).to eq(['No data generated'])
        end

        it 'returns false for non-zero exit status' do
          allow(process_status).to receive(:exitstatus).and_return(1)

          success, _ = described_class.run_k6(**base_params)
          expect(success).to be false
        end

        it 'returns false for signal termination' do
          allow(process_status).to receive_messages(signaled?: true, termsig: 123)
          allow(Open3).to receive(:popen2e).and_yield(stdin, [].each, wait_thread)

          success, _ = described_class.run_k6(**base_params)

          expect(success).to be false
        end
      end
    end

    context 'with error handling' do
      shared_examples 'k6 configuration error' do |error_type|
        before do
          allow(Open3).to receive(:popen2e).and_yield(stdin, error_message, wait_thread)
        end

        let(:error_message) { [%(GoError: #{error_type}: ")].each }

        it('raises ArgumentError') do
          expect { described_class.run_k6(**base_params) }.to raise_error(ArgumentError)
        end
      end

      it_behaves_like 'k6 configuration error', 'Missing Project Config Data'
      it_behaves_like 'k6 configuration error', 'Missing Environment Variable'

      context 'with normal output' do
        let(:error_message) { ['Some normal output', 'Another line'].each }

        it 'does not raise for non-error output' do
          expect { described_class.run_k6(**base_params) }.not_to raise_error
        end
      end
    end
  end
  # rubocop:enable RSpec/MultipleMemoizedHelpers

  describe '.get_test_results' do
    let(:test_file) { 'test/performance/api_test.js' }
    let(:status) { true }
    let(:test_redo) { false }

    before do
      allow(TestInfo).to receive(:get_test_tag_value)
                           .with(test_file, 'issue').and_return(['ISSUE-123'])
      allow(TestInfo).to receive(:get_test_tag_value)
                           .with(test_file, 'flags').and_return(%w[flag1 flag2])
    end

    context 'with complete output' do
      let(:output) do
        [
          '  script: api_test.js',
          'http_req_waiting.......: avg=123.45ms p(90)=234.56ms p(95)=345.67ms',
          'data_received..........: 456.7 MB 789.0 MB/s',
          'Up to 100 iterations',
          'RPS Threshold: 50.5/s',
          'TTFB P90 Threshold: 200ms',
          'http_reqs.............: 45.67/s',
          'Success Rate Threshold: 99.9%',
          'successful_requests...: 98.5%'
        ]
      end

      it 'parses all metrics correctly', :aggregate_failures do
        results = described_class.get_test_results(
          test_file: test_file,
          status: status,
          output: output,
          test_redo: test_redo
        )

        expect(results["name"]).to eq("api_test")
        expect(results["ttfb_avg"]).to eq("123.45")
        expect(results["ttfb_p90"]).to eq("234.56")
        expect(results["ttfb_p95"]).to eq("345.67")
        expect(results["rps_target"]).to eq("100")
        expect(results["rps_threshold"]).to eq("50.5")
        expect(results["ttfb_p90_threshold"]).to eq("200")
        expect(results["rps_result"]).to eq("45.67")
        expect(results["success_rate_threshold"]).to eq("99.9")
        expect(results["success_rate"]).to eq("98.5")
        expect(results["result"]).to be(true)
        expect(results["redo"]).to be(false)
        expect(results["issues"]).to eq(['ISSUE-123'])
        expect(results["flags"]).to eq(%w[flag1 flag2])
      end

      it 'calculates score correctly' do
        results = described_class.get_test_results(
          test_file: test_file,
          status: status,
          output: output,
          test_redo: test_redo
        )

        # Score = (rps_result / rps_target) * success_rate
        # (45.67 / 100) * 98.5 = 44.98
        expect(results["score"]).to eq(44.98)
      end
    end

    context 'with missing data' do
      let(:output) do
        [
          '  script: api_test.js',
          'http_req_waiting.......: avg=123.45ms p(90)=234.56ms p(95)=345.67ms'
        ]
      end

      it 'handles missing metrics gracefully' do
        results = described_class.get_test_results(
          test_file: test_file,
          status: status,
          output: output,
          test_redo: test_redo
        )

        expect(results["score"]).to eq(0.0)
      end
    end

    context 'with score exceeding 100' do
      # When RPS result (100) is double the target (50) and success rate is 100%
      # Score would be (100/50 * 100) = 200%, but should be capped at 100 as system can't be 'better' than target goal
      let(:output) do
        [
          '  script: api_test.js',
          'Up to 50 iterations',
          'http_reqs.............: 100.0/s',
          'successful_requests...: 100.0%'
        ]
      end

      it 'caps score at 100' do
        results = described_class.get_test_results(
          test_file: test_file,
          status: status,
          output: output,
          test_redo: test_redo
        )

        expect(results["score"]).to eq(100.0)
      end
    end

    context 'with malformed output lines' do
      let(:output) do
        [
          'script: invalid#name.js',
          'http_req_waiting.......: avg=invalid p(90)=invalid p(95)=invalid',
          'data_received..........: invalid_data',
          'Up to invalid',
          'RPS Threshold: invalid/s',
          'TTFB P90 Threshold: invalid',
          'http_reqs.............: invalid/s',
          'Success Rate Threshold: invalid%',
          'successful_requests...: invalid%'
        ]
      end

      it 'raises NoMethodError when regex matches fail' do
        # TODO: Consider adding better error handling in the regexp matchers
        # Currently crashes with NoMethodError when trying to access capture groups on nil
        expect do
          described_class.get_test_results(
            test_file: test_file,
            status: status,
            output: output,
            test_redo: test_redo
          )
        end.to raise_error(NoMethodError)
      end
    end

    context 'with test_redo flag' do
      let(:output) { ['script: api_test.js'] }
      let(:test_redo) { true }

      it 'sets redo flag correctly' do
        results = described_class.get_test_results(
          test_file: test_file,
          status: status,
          output: output,
          test_redo: test_redo
        )

        expect(results["redo"]).to be(true)
      end
    end
  end

  describe '.get_results_score' do
    let(:env_vars) { { 'RPS_THRESHOLD_MULTIPLIER' => '0.8' } }

    context 'with valid results' do
      it 'calculates average score' do
        results = [
          { 'score' => 80.0, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => 90.0, 'rps_threshold' => '40', 'rps_target' => '50' }
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        # Average of 80 and 90 = 85
        expect(score).to eq(85.0)
      end

      it 'caps total score at 100' do
        results = [
          { 'score' => 100.0, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => 110.0, 'rps_threshold' => '40', 'rps_target' => '50' }
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to eq(100.0)
      end
    end

    context 'when filtering results' do
      it 'ignores results with nil scores' do
        results = [
          { 'score' => 80.0, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => nil, 'rps_threshold' => '40', 'rps_target' => '50' }
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to eq(80.0)
      end

      it 'ignores results where threshold is below target × multiplier' do
        results = [
          { 'score' => 80.0, 'rps_threshold' => '40', 'rps_target' => '50' },  # 40 >= 50 * 0.8 = 40 (included)
          { 'score' => 90.0, 'rps_threshold' => '35', 'rps_target' => '50' }   # 35 < 50 * 0.8 = 40 (excluded)
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to eq(80.0)
      end
    end

    context 'with edge cases' do
      it 'returns nil when all results are filtered out' do
        results = [
          { 'score' => nil, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => 90.0, 'rps_threshold' => '35', 'rps_target' => '50' } # below threshold
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to be_nil
      end

      it 'returns nil for empty results array' do
        score = described_class.get_results_score(results: [], env_vars: env_vars)
        expect(score).to be_nil
      end

      it 'handles integer scores' do
        results = [
          { 'score' => 80, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => 90, 'rps_threshold' => '40', 'rps_target' => '50' }
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to eq(85.0)
      end
    end

    context 'with need for precision handling' do
      it 'rounds final score to 2 decimal places' do
        results = [
          { 'score' => 80.333, 'rps_threshold' => '40', 'rps_target' => '50' },
          { 'score' => 90.666, 'rps_threshold' => '40', 'rps_target' => '50' }
        ]

        score = described_class.get_results_score(results: results, env_vars: env_vars)
        expect(score).to eq(85.50)
      end
    end
  end

  describe '.restore_environment_rate_limit_settings' do
    let(:environment_url) { 'https://test.gitlab.com' }
    let(:env_vars) { { 'ENVIRONMENT_URL' => environment_url } }
    let(:access_token) { 'dummy_token' }
    let(:original_settings) do
      {
        'request_rate_limit' => 1000,
        'other_rate_limit' => 2000,
        'non_limit_setting' => 'value'
      }
    end

    let(:current_settings) do
      {
        'request_rate_limit' => 2000,        # Different from original AND has 'limit'
        'other_rate_limit' => 2000,          # Same as original AND has 'limit'
        'non_limit_setting' => 'different'   # Different but NO 'limit'
      }
    end

    before do
      allow(GPTLogger.logger).to receive(:info)
      allow(Rainbow).to receive(:yellow)
      allow_any_instance_of(Object).to receive(:sleep)
      ENV['ACCESS_TOKEN'] = access_token
    end

    context 'when settings restoration succeeds' do
      before do
        allow(GPTCommon).to receive(:get_env_settings).and_return(current_settings)
        allow(GPTCommon).to receive(:change_env_settings)
      end

      it 'restores only the changed rate limit settings' do
        described_class.restore_environment_rate_limit_settings(
          env_vars: env_vars,
          gitlab_env_settings: original_settings
        )

        expect(GPTCommon).to have_received(:change_env_settings)
          .with(
            env_url: environment_url,
            headers: { 'PRIVATE-TOKEN': access_token },
            settings: { request_rate_limit: 1000 }
          )
          .once
      end
    end

    context 'when initial settings are empty' do
      it 'returns early without fetching current settings' do
        allow(GPTCommon).to receive(:get_env_settings)

        described_class.restore_environment_rate_limit_settings(
          env_vars: env_vars,
          gitlab_env_settings: {}
        )

        expect(GPTCommon).not_to have_received(:get_env_settings)
      end
    end

    context 'when errors occur' do
      shared_examples 'retries once on error' do |error_class|
        it "retries once on #{error_class}" do
          call_count = 0
          allow(GPTCommon).to receive(:get_env_settings) do
            call_count += 1
            raise error_class if call_count == 1

            current_settings
          end

          allow(GPTCommon).to receive(:change_env_settings)

          described_class.restore_environment_rate_limit_settings(
            env_vars: env_vars,
            gitlab_env_settings: original_settings
          )

          expect(call_count).to eq(2)
        end
      end

      it_behaves_like 'retries once on error', StandardError
      it_behaves_like 'retries once on error', JSON::ParserError

      it 'logs a message when settings fetch fails' do
        allow(GPTCommon).to receive(:get_env_settings).and_return({})

        described_class.restore_environment_rate_limit_settings(
          env_vars: env_vars,
          gitlab_env_settings: original_settings
        )

        expect(GPTLogger.logger).to have_received(:info).with(
          including("Skipping restoring the rate limit settings due to an environment error")
        )
      end
    end
  end
end
