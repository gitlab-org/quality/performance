/*global __ENV : true  */
/*
@endpoint: `POST /api/graphql`
@description: Request an AI summary for a new MR
@gitlab_settings: { "instance_level_ai_beta_features_enabled": true }
@gpt_data_version: 1
@stressed_components: Rails, Postgres
*/

import http from 'k6/http';
import { check, group } from 'k6';
import { Rate } from 'k6/metrics';
import {
    logError,
    getRpsThresholds,
    getTtfbThreshold, selectRandom, getLargeProjects,
} from "../../lib/gpt_k6_modules.js";

export let thresholds = {
    ttfb: { latest: 1000 },
};

export let rpsThresholds = getRpsThresholds(thresholds["rps"]);
export let ttfbThreshold = getTtfbThreshold(thresholds["ttfb"]);
export let successRate = new Rate("successful_requests");

export let options = {
    thresholds: {
        successful_requests: [`rate>${__ENV.SUCCESS_RATE_THRESHOLD}`],
        http_req_waiting: [`p(90)<${ttfbThreshold}`],
        http_reqs: [`count>=${rpsThresholds["count"]}`],
    },
};

export let projects = getLargeProjects(['encoded_path', 'branch', 'default_branch']);

// If Service Account PAT is used for GPT, AI tests require real user PAT which can be provided via AI_ACCESS_TOKEN
export const access_token =
    __ENV.AI_ACCESS_TOKEN !== null && __ENV.AI_ACCESS_TOKEN !== undefined
        ? __ENV.AI_ACCESS_TOKEN
        : __ENV.ACCESS_TOKEN;

export function setup() {
    console.log("");
    console.log(
        `RPS Threshold: ${rpsThresholds["mean"]}/s (${rpsThresholds["count"]})`,
    );
    console.log(`TTFB P90 Threshold: ${ttfbThreshold}ms`);
    console.log(
        `Success Rate Threshold: ${parseFloat(__ENV.SUCCESS_RATE_THRESHOLD) * 100}%`,
    );

    projects.forEach(project => {
        project.projectPath = project['encoded_path'];
        project.projectId = get_project_id(project.projectPath);
        project.targetBranch = project['default_branch'];
        project.sourceBranch = selectRandom(project['branch']);
    });
    return { projects };

}
export default function (data) {
    group("API - Summarize New Merge Request Query", function () {
        const project = selectRandom(data.projects);

        let params = {
           headers: {
               "Content-Type": "application/json",
               Authorization: `Bearer ${access_token}`,
           },
        };

        let gql_query = `
          mutation {
            aiAction(
              input: {
                summarizeNewMergeRequest: {
                    resourceId: "gid://gitlab/Project/${project.projectId}",
                  sourceBranch: "${project.sourceBranch}"
                  targetBranch: "${project.targetBranch}"
                }
              }
            ){
              requestId
              errors
              clientMutationId
            }
          }`;

        let res = http.post(
            `${__ENV.ENVIRONMENT_URL}/api/graphql`,
            JSON.stringify({ query: gql_query }),
            params,
        );

        const checkOutput = check(res, {
            'status is 200': (r) => r.status === 200,
            'verify response request-id is not null': (r) => typeof JSON.parse(r.body).data.aiAction.requestId !== "undefined",
        });
        checkOutput ? successRate.add(true) : (successRate.add(false), logError(res));
    });
}

function get_project_id(projectPath) {
    const res = http.get(
        `${__ENV.ENVIRONMENT_URL}/api/v4/projects/${projectPath}`,
        { headers: { "Accept": "application/json", "PRIVATE-TOKEN": `${__ENV.ACCESS_TOKEN}` } }
    );
    const projectId = res.json()["id"]
    return projectId
}
